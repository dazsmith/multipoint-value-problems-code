# Multipoint value problems: code

Code to evaluate solutions of initial-multipoint value problems, as obtained via the Fokas method, or unified transform method.

The code is written in Julia, in an IJulia notebook.
